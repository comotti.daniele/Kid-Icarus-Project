// Copyright Epic Games, Inc. All Rights Reserved.

#include "KidIcarusProjectCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "BowComponent.h"

AKidIcarusProjectCharacter::AKidIcarusProjectCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate when the controller rotates.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Create a camera boom attached to the root (capsule)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f,0.f,75.f);
	CameraBoom->SetRelativeRotation(FRotator(0.f,180.f,0.f));

	// Create a camera and attach to boom
	SideViewCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("SideViewCamera"));
	SideViewCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	SideViewCameraComponent->bUsePawnControlRotation = false; // We don't want the controller rotating the camera

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Face in the direction we are moving..
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 720.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->GravityScale = 2.f;
	GetCharacterMovement()->AirControl = 0.80f;
	GetCharacterMovement()->JumpZVelocity = 1000.f;
	GetCharacterMovement()->GroundFriction = 3.f;
	GetCharacterMovement()->MaxWalkSpeed = 600.f;
	GetCharacterMovement()->MaxFlySpeed = 600.f;

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)

	BowComp = CreateDefaultSubobject<UBowComponent>("Bow Comp");

	isCrouching = false;
	isAimingUpwards = false;
	PlayerMaxHealth = 8.00f;
	PlayerHealth = 8.00f;
	ColHearts;
	Points;
	BestHearts;
	BestPoints;
	LastHearts;
	LastPoints;
	Strength = 1;

	BottomLimit;
	LeftLimit;
	RightLimit;
	ScreenCentre;
	ScreenDifference;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AKidIcarusProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAxis("MoveRight", this, &AKidIcarusProjectCharacter::MoveRight);
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &AKidIcarusProjectCharacter::Crouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &AKidIcarusProjectCharacter::StopCrouch);
	PlayerInputComponent->BindAction("Aim up", IE_Pressed, this, &AKidIcarusProjectCharacter::AimUp);
	PlayerInputComponent->BindAction("Aim up", IE_Released, this, &AKidIcarusProjectCharacter::StopAimUp);

	PlayerInputComponent->BindTouch(IE_Pressed, this, &AKidIcarusProjectCharacter::TouchStarted);
	PlayerInputComponent->BindTouch(IE_Released, this, &AKidIcarusProjectCharacter::TouchStopped);
}

void AKidIcarusProjectCharacter::MoveRight(float Value)
{
	// add movement in that direction
	if (!isAimingUpwards&&!isCrouching)
	{
		AddMovementInput(FVector(0.f, -1.f, 0.f), Value);
	}
	
}

void AKidIcarusProjectCharacter::TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	// jump on any touch
	Jump();
}

void AKidIcarusProjectCharacter::TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	StopJumping();
}

void AKidIcarusProjectCharacter::Crouch()
{
	isCrouching = true;
	GetCapsuleComponent()->SetCapsuleHalfHeight(48.0f);
}
void AKidIcarusProjectCharacter::StopCrouch()
{
	isCrouching = false;
	GetCapsuleComponent()->SetCapsuleHalfHeight(96.0f);
}

void AKidIcarusProjectCharacter::AimUp()
{
	isAimingUpwards = true;
}
void AKidIcarusProjectCharacter::StopAimUp()
{
	isAimingUpwards = false;
}

void AKidIcarusProjectCharacter::GameOverScene()
{
	UGameplayStatics::OpenLevel(this, "GameOverMap");
}

void AKidIcarusProjectCharacter::TakeDamage(float _damageAmount)
{
	PlayerHealth -= _damageAmount;
	if (PlayerHealth < 0)
	{
		PlayerHealth = 0;
		GameOverScene();
	}
}
void AKidIcarusProjectCharacter::HealLife(float _regainedHealth)
{
	PlayerHealth += _regainedHealth;
	if (PlayerHealth >= PlayerMaxHealth)
	{
		PlayerHealth = PlayerMaxHealth;
	}
}

void AKidIcarusProjectCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector MyCharacter = GetWorld()->GetFirstPlayerController()->GetPawn()->GetActorLocation();


	BottomLimit = ScreenCentre - ScreenDifference;

	if (MyCharacter.Z < BottomLimit)
	{
		PlayerHealth = 0;
	}

	if (MyCharacter.Y < RightLimit)
	{
		MyCharacter.Y = LeftLimit;

		const FVector pos = FVector(FGenericPlatformMath::TruncToInt(MyCharacter.X), FGenericPlatformMath::TruncToInt(LeftLimit), FGenericPlatformMath::TruncToInt(MyCharacter.Z));
		SetActorLocation(pos, false, 0, ETeleportType::TeleportPhysics);
	}
	if (MyCharacter.Y > LeftLimit)
	{
		MyCharacter.Y = RightLimit;
		const FVector pos = FVector(FGenericPlatformMath::TruncToInt(MyCharacter.X), FGenericPlatformMath::TruncToInt(RightLimit), FGenericPlatformMath::TruncToInt(MyCharacter.Z));
		SetActorLocation(pos, false, 0, ETeleportType::TeleportPhysics);
	}

	if (MyCharacter.Z > ScreenCentre)
	{
		ScreenCentre = MyCharacter.Z;
	}
}