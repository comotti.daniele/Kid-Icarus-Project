// Copyright Epic Games, Inc. All Rights Reserved.

#include "KidIcarusProjectGameMode.h"
#include "KidIcarusProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

AKidIcarusProjectGameMode::AKidIcarusProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/SideScrollerCPP/Blueprints/SideScrollerCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
