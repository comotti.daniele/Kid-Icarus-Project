// Fill out your copyright notice in the Description page of Project Settings.


#include "WaterOfLife.h"
#include "Components/CapsuleComponent.h"

// Sets default values
AWaterOfLife::AWaterOfLife()
{
	// Set size for collision capsule
	//GetCapsuleComponent()->InitCapsuleSize(10.f, 20.0f);

 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GlassCollision=CreateDefaultSubobject<UCapsuleComponent>("Glass Collision");

	LifeGiven = 7.00f;

}

// Called when the game starts or when spawned
void AWaterOfLife::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWaterOfLife::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

