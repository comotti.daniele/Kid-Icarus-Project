// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "KIGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class KIDICARUSPROJECT_API UKIGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
};
