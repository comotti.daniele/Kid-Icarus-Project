// Fill out your copyright notice in the Description page of Project Settings.


#include "KIGameInstanceSubsystem.h"
#include "Kismet/GameplayStatics.h"
#include "KISaveGame.h"
#include "KISaveGameDeveloperSettings.h"
#include "KidIcarusProjectCharacter.h"


bool UKIGameInstanceSubsystem::Save(const FString& saveName, const int32 index)
{
	UKISaveGameDeveloperSettings* devSettings = GetMutableDefault< UKISaveGameDeveloperSettings>();

	const TSubclassOf<UKISaveGame> savegameType = devSettings->GetSaveGameClass();
	USaveGame* SaveGameObject = UGameplayStatics::CreateSaveGameObject(savegameType);

	AKidIcarusProjectCharacter* pit = Cast<AKidIcarusProjectCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (pit && SaveGameObject)
	{
		//const AKidIcarusProjectCharacter* points = pawn->FindComponentByClass<AKidIcarusProjectCharacter>();
		UKISaveGame* SaveGameKI = Cast<UKISaveGame> (SaveGameObject);
		if (SaveGameKI)
		{
			if (pit)
			{
				SaveGameKI->PlayerHearts = pit->ColHearts;
				SaveGameKI->PlayerPoints = pit->Points;
			}
			if (pit->ColHearts >= SaveGameKI->BestHearts)
			{
				SaveGameKI->BestHearts = pit->ColHearts;
				pit->BestHearts = pit->ColHearts;
			}
			if (pit->Points >= SaveGameKI->BestPoints)
			{
				SaveGameKI->BestPoints = pit->Points;
				pit->BestPoints = pit->Points;
			}
		}
		return UGameplayStatics::SaveGameToSlot(SaveGameObject, saveName, index);
	}
	return false;
}

void UKIGameInstanceSubsystem::Load(const FString& saveName, const int32 index)
{
	USaveGame* SaveGameObject = UGameplayStatics::LoadGameFromSlot(saveName, index);

	AKidIcarusProjectCharacter* pit = Cast<AKidIcarusProjectCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (pit && SaveGameObject)
	{
		//const AKidIcarusProjectCharacter* points = pawn->FindComponentByClass<AKidIcarusProjectCharacter>();
		UKISaveGame* SaveGameKI = Cast<UKISaveGame>(SaveGameObject);
		if (SaveGameKI)
		{
			if (pit)
			{
				pit->BestHearts = SaveGameKI->BestHearts;
				pit->BestPoints = SaveGameKI->BestPoints;
				pit->LastHearts = SaveGameKI->PlayerHearts;
				pit->LastPoints = SaveGameKI->PlayerPoints;
			}
		}
	}
}