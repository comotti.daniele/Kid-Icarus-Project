// Fill out your copyright notice in the Description page of Project Settings.


#include "KIArrow.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/CapsuleComponent.h"

// Sets default values
AKIArrow::AKIArrow()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>("Projectile Movement");
	ProjectileCollision = CreateDefaultSubobject<UCapsuleComponent>("Projectile Collision");

	Strength;

}

// Called when the game starts or when spawned
void AKIArrow::BeginPlay()
{
	Super::BeginPlay();
	
}



