// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Shemum.generated.h"

UCLASS()
class KIDICARUSPROJECT_API AShemum : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AShemum();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	//UEnemies* EnemComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Drop)
		TSubclassOf<AActor> ActorToDrop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		float Life;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
		float Score;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UCapsuleComponent* HitCollision;


public:	

	UFUNCTION(BlueprintCallable, Category = Drop)
		void DropItem();

	UFUNCTION(BlueprintCallable)
		void TakeDamage(float _damageAmount);

	// Called every frame
	//virtual void Tick(float DeltaTime) override;

};
