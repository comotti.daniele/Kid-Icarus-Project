// Fill out your copyright notice in the Description page of Project Settings.


#include "Shemum2.h"
#include "Components/CapsuleComponent.h"

// Sets default values
AShemum2::AShemum2()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//HitCollision = CreateDefaultSubobject<UCapsuleComponent>("Hit Collision");
	GetCapsuleComponent()->InitCapsuleSize(30.f, 96.0f);

	Life;
	Damage;
	Score;

	LeftLimit;
	RightLimit;

}

// Called when the game starts or when spawned
void AShemum2::BeginPlay()
{
	Super::BeginPlay();
	
}



// Called every frame
void AShemum2::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector MySelf = GetActorLocation();

	if (MySelf.Y < RightLimit)
	{
		MySelf.Y = LeftLimit;

		const FVector pos = FVector(FGenericPlatformMath::TruncToInt(MySelf.X), FGenericPlatformMath::TruncToInt(LeftLimit), FGenericPlatformMath::TruncToInt(MySelf.Z));
		SetActorLocation(pos, false, 0, ETeleportType::TeleportPhysics);
	}
	if (MySelf.Y > LeftLimit)
	{
		MySelf.Y = RightLimit;
		const FVector pos = FVector(FGenericPlatformMath::TruncToInt(MySelf.X), FGenericPlatformMath::TruncToInt(RightLimit), FGenericPlatformMath::TruncToInt(MySelf.Z));
		SetActorLocation(pos, false, 0, ETeleportType::TeleportPhysics);
	}

}

// Called to bind functionality to input
void AShemum2::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

