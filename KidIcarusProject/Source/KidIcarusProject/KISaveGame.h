// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "KISaveGame.generated.h"

/**
 * 
 */
UCLASS()
class KIDICARUSPROJECT_API UKISaveGame : public USaveGame
{
	GENERATED_BODY()

public:

	UPROPERTY(BlueprintReadOnly, SaveGame, Category = Save)
	float PlayerHearts;
	UPROPERTY(BlueprintReadOnly, SaveGame, Category = Save)
	float BestHearts;
	UPROPERTY(BlueprintReadOnly, SaveGame, Category = Save)
	float PlayerPoints;
	UPROPERTY(BlueprintReadOnly, SaveGame, Category = Save)
	float BestPoints;

};
