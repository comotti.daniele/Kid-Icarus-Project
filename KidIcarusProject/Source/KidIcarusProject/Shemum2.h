// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Enemies.h"
#include "Shemum2.generated.h"

UCLASS()
class KIDICARUSPROJECT_API AShemum2 : public AEnemies
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShemum2();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Drop)
	//	TSubclassOf<AActor> ActorToDrop;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
	//	float Life;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
	//	float Damage;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
	//	float Score;

	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	//	class UCapsuleComponent* HitCollision;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position values")
	float LeftLimit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position values")
	float RightLimit;

public:	

	//UFUNCTION(BlueprintCallable, Category = Drop)
	//	void DropItem(const FVector& initialPosition, const FRotator& initialRotation);
	//
	//UFUNCTION(BlueprintCallable)
	//	void TakeDamage(float _damageAmount);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
