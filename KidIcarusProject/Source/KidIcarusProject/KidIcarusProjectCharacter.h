// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "KidIcarusProjectCharacter.generated.h"

class UBowComponent;

UCLASS(config=Game)
class AKidIcarusProjectCharacter : public ACharacter
{
	GENERATED_BODY()

protected:
	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GPC)
	UBowComponent* BowComp;

	//UFUNCTION(BlueprintCallable, Category = Actions)
	void Crouch();
	void StopCrouch();
	void AimUp();
	void StopAimUp();

	UFUNCTION(BlueprintCallable)
	void GameOverScene();

	//void SetSCValue();
	//void TPLeft();
	//void TPRight();

protected:

	/** Called for side to side input */
	void MoveRight(float Val);

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	UFUNCTION(BlueprintCallable)
	void TakeDamage(float _damageAmount);
	UFUNCTION(BlueprintCallable)
	void HealLife(float _regainedHealth);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
	bool isCrouching;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status")
	bool isAimingUpwards;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float PlayerMaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float PlayerHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position values")
	float BottomLimit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position values")
	float LeftLimit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position values")
	float RightLimit;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position values")
	float ScreenCentre;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Position values")
	float ScreenDifference;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Strength")
	float Strength;

public:
	AKidIcarusProjectCharacter();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
	float Points;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
	float ColHearts;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
	float BestPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
	float BestHearts;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
	float LastPoints;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Points")
	float LastHearts;

	UFUNCTION(BlueprintCallable, Category = "Points")
	void GainPoints(float _plusPoints)
	{
		Points += _plusPoints;
	}

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

	virtual void Tick(float DeltaTime) override;
};
