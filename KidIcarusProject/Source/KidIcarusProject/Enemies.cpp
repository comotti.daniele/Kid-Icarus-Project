// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemies.h"
#include "KidIcarusProjectCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values for this component's properties
AEnemies::AEnemies()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.

	// ...
}


// Called when the game starts
void AEnemies::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void AEnemies::TakeDamage(float _damageAmount)
{
	Life -= _damageAmount;
	if (Life <= 0)
	{
		Life = 0;

		FVector MySelf = GetActorLocation();
		DropItem(MySelf, FRotator(0.f, 0.f, 0.f));

	}
}

void AEnemies::DropItem(const FVector& initialPosition, const FRotator& initialRotation)
{
	AActor* heart = GetWorld()->SpawnActor<AActor>(ActorToDrop, initialPosition, initialRotation, FActorSpawnParameters());

	AKidIcarusProjectCharacter* player = Cast<AKidIcarusProjectCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (player)
	{
		player->GainPoints(Score);
	}

	Destroy();
}
