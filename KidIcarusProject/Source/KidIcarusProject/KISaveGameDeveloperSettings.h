// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DeveloperSettings.h"
#include "KISaveGame.h"
#include "KISaveGameDeveloperSettings.generated.h"

/**
 * 
 */
UCLASS()
class KIDICARUSPROJECT_API UKISaveGameDeveloperSettings : public UDeveloperSettings
{
	GENERATED_BODY()
	
protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Config)
	TSubclassOf<UKISaveGame> SaveGameClass;

public:

	UFUNCTION(BlueprintCallable, Category = Config)
	TSubclassOf<UKISaveGame> GetSaveGameClass() const
	{
		return SaveGameClass;
	}

};
