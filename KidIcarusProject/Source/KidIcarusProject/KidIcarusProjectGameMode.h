// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "KidIcarusProjectGameMode.generated.h"

UCLASS(minimalapi)
class AKidIcarusProjectGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AKidIcarusProjectGameMode();
};



