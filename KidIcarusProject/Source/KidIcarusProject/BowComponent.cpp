// Fill out your copyright notice in the Description page of Project Settings.


#include "BowComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "KIArrow.h"

// Sets default values for this component's properties
UBowComponent::UBowComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBowComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UBowComponent::Shoot(const FVector& initialPosition, const FRotator& initialRotation, const FVector& initialSpeed, const float& strength)
{
	AActor* arrow = GetWorld()->SpawnActor<AActor>(ActorToShoot, initialPosition, initialRotation, FActorSpawnParameters());
	if (arrow)
	{
		UProjectileMovementComponent* arrowComp = arrow->FindComponentByClass<UProjectileMovementComponent>();

		AKIArrow* generatedArrow = Cast<AKIArrow>(arrow);
		if (generatedArrow != nullptr)
		{
			generatedArrow->SetUpStat(strength);
		}

		if (arrowComp)
		{
			//arrowComp->
		}
	}
}


// Called every frame
//void UBowComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
//{
//	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
//
//	// ...
//}

