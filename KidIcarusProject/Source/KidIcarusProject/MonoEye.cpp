// Fill out your copyright notice in the Description page of Project Settings.


#include "MonoEye.h"
#include "Components/CapsuleComponent.h"

// Sets default values
AMonoEye::AMonoEye()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(30.f, 30.0f);

	Life;
	Damage;
	Score;

}

// Called when the game starts or when spawned
void AMonoEye::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void AMonoEye::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AMonoEye::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

