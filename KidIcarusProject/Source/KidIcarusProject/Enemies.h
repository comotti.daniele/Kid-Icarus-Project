// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "GameFramework/Character.h"
#include "Enemies.generated.h"


UCLASS( ClassGroup=(Custom))
class KIDICARUSPROJECT_API AEnemies : public ACharacter
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	AEnemies();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Drop)
	TSubclassOf<AActor> ActorToDrop;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
	float Life;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
	float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Stats)
	float Score;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	class UCapsuleComponent* HitCollision;

public:	

	UFUNCTION(BlueprintCallable, Category = Drop)
	void DropItem(const FVector& initialPosition, const FRotator& initialRotation);

	UFUNCTION(BlueprintCallable)
	void TakeDamage(float _damageAmount);

	// Called every frame
	//virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
