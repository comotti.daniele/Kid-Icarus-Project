// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class KidIcarusProject : ModuleRules
{
	public KidIcarusProject(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore" });
	}
}
