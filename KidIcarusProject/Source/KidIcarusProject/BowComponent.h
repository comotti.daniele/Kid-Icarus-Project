// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BowComponent.generated.h"


UCLASS( ClassGroup=(KI), meta=(BlueprintSpawnableComponent) )
class KIDICARUSPROJECT_API UBowComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBowComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Shoot)
	TSubclassOf<AActor> ActorToShoot;

public:	

	UFUNCTION(BlueprintCallable, Category = Shoot)
	void Shoot(const FVector& initialPosition, const FRotator& initialRotation, const FVector& initialSpeed, const float& strength);

};
