// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "KIGameInstanceSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class KIDICARUSPROJECT_API UKIGameInstanceSubsystem : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = Save)
	bool Save(const FString& saveName, const int32 index);

	UFUNCTION(BlueprintCallable, Category = Save)
	void Load(const FString& saveName, const int32 index);

};
