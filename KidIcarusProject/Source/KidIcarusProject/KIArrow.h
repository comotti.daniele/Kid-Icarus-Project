// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "KIArrow.generated.h"

UCLASS()
class KIDICARUSPROJECT_API AKIArrow : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AKIArrow();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GPC)
	class UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = GPC)
	class UCapsuleComponent* ProjectileCollision;


public:	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Strength")
	float Strength;

	UFUNCTION(BlueprintCallable, Category = "Strength")
	void SetUpStat(float _givenStrength)
	{
		Strength = _givenStrength;
	}

};
